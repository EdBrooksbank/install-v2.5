#!/usr/bin/env bash

# version
GIT_RELEASE="ccpnmr2.5.3"
RELEASE_VERSION="2.5.3"

# miniconda source
CONDA_SOURCE="ccpn-v2.5.3"
