## Install Development

**This repository contains the script for installing the developer's version of CcpNmr V2.5**

More information on v2.5 can be found on the [CCPN website](https://www.ccpn.ac.uk).

---

## Running the script

Usage: *./installDevelopment.sh* with the following switches:

-cC &nbsp;&nbsp;&nbsp;&nbsp;clone repositories\
-eE &nbsp;&nbsp;&nbsp;&nbsp;include existing repositories\
-gG &nbsp;&nbsp;&nbsp;&nbsp;checkout the release branch defined in *version.sh*\
-p &lt;path&gt; &nbsp;&nbsp;&nbsp;&nbsp;specify download path\
-P  &nbsp;&nbsp;&nbsp;&nbsp;use default path\
-kK &nbsp;&nbsp;&nbsp;&nbsp;generate ssh-keygen for git/bitbucket, this will open the bitbucket website\
-h &nbsp;&nbsp;&nbsp;&nbsp;display help\
-r &lt;remote&gt; &nbsp;&nbsp;&nbsp;&nbsp;specify name for remote\
-R &nbsp;&nbsp;&nbsp;&nbsp;use default remote [origin]\
-F &nbsp;&nbsp;&nbsp;&nbsp;force create project path

Use uppercase to set True, lowercase to set to False where the option is available.
Ignored options will request input.

To clone all repositories without user input:

*./installDevelopment.sh -CEGPkRF*

---

The project can contain more than one repository, including nested repositories.
These are specified in *projectSettings.sh*.
The git branch for checkout after cloning can be set in *version.sh*.
